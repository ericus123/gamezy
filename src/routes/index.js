import {BrowserRouter as Router , Route, Switch} from "react-router-dom";
import { RouteNames } from "../config/RouteNames";
import GamePage from "../pages/GamePage";

const Routes = () => {
return (
    <Router>
        <Switch>
            <Route  exact path={RouteNames.home} component={GamePage}/>
        </Switch>
    </Router>
);
};

export default Routes;