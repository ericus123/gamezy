import {useState } from "react";
import { GenerateColor } from "../helpers/GenerateColor";
import all_colors from "../utils/colors.json";
import "../styles/scss/project_theme.scss";
import "./index.scss";


const GamePage  = () => {

    const [color, setColor] = useState("#FF0000");
    const [left , setLeft] = useState(100);
     const [top , setTop] = useState(100);

    const handleChange = () => {
        setColor(GenerateColor(all_colors));
        setLeft(Math.floor( Math.random() * 400 ));
        setTop(Math.floor( Math.random() * 400 ));
    };

    return (
        <>

         <h1 className="game-title" style={{color:color}}>.GAMEZY.</h1>
        <div className="gamepage-wrapper">

     
<div  onClick={handleChange} style={{backgroundColor:color, left: left,top: top,}}  className={" square-box"}>

</div>
        </div>
        </>
    );
};

export default GamePage;